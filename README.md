#Add pane to all overriden panelized nodes of a node type.

In this example we will add the 'events-events_promo' view pane to the end of the region 'title' for all overriden panelized nodes of node type 'article' that are using the layout 'hab_article'.

_Note that setting $pane->configuration is not required. see "Pane with configuration" chapter below._

	$pane = panels_new_pane('views_panes', 'events-events_promo');
 
	$pane->configuration = array (
	  'override_title' => 0,
	  'override_title_text' => '',
	);
 
	_panelizer_bulk_update_add_pane_to_panelizer_nodes('article', $pane, 'hab_article', 'title');

#Set configuration or style

_To add style settings instead, replace the word **configuration** with **style** in the instructions below._

Panels stores the specific pane configuration and style settings in a serialized array in the _panels_pane_ table. One way to find the array structure is to find an already added pane of the same type, subtype and configuration in the _panels_pane_ table.

1. Find a pane of same type and configuration (Or add one through the gui) in the _panels_pane_ table.
2. Copy the serialized array from the configuration column.
3. Unserialize the string (For example [here](http://php.fnlist.com/php/unserialize))
4. Set $pane->configuration to the unserialized array before adding it. (See "Add pane to overriden panelized nodes" chapter above.)